[![Moleculer logo](./banner-moleculer.png)](https://moleculer.services/)

[![pipeline status](https://gitlab.com/warboard1/moleculer-cqrs-domains/badges/master/pipeline.svg)](https://gitlab.com/warboard1/moleculer-cqrs-domains/commits/master)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![coverage report](https://gitlab.com/warboard1/moleculer-cqrs-domains/badges/master/coverage.svg)](https://gitlab.com/warboard1/moleculer-cqrs-domains/commits/master)

## Anatomy of a "COMMAND" life cycle

Here the steps to resolve a command in this architecture:

#### Call "MoleculerService.Action"
Simple call an action with moleculer

#### Hook "ExtractActionMetadata"
The action is a part of CQRS or not ?

#### Hook "ValidateCQRSParams"
The action is a "Command" or a "Query" ? Yes, so we need to validate the params

#### Hook "EmitEventSourcing"
 The action is a "Command" ? Yes, so we need to create in eventstore

#### Hook "ResponseCQRS"
The action is a "Command" or a "Query" ? It's a success!

#### Hook "ErrorCQRS"
The action is a "Command" or a "Query" ? Oh no, there is an error!

## Anatomy of a "QUERY" life cycle

Here the steps to resolve a query in this architecture:

#### Call "MoleculerService.Action"
Simple call an action with moleculer

#### Hook "ExtractActionMetadata"
The action is a part of CQRS or not ?

#### Hook "ValidateCQRSParams"
The action is a "Command" or a "Query" ? Yes, so we need to validate the params

#### Hook "ResponseCQRS"
The action is a "Command" or a "Query" ? It's a success!

#### Hook "ErrorCQRS"
The action is a "Command" or a "Query" ? Oh no, there is an error!
