const DbService = require('moleculer-db')
const MongoDBAdapter = require('moleculer-db-adapter-mongo')
const { Rabbit } = require('rabbit-queue')

const { rabbitmq, mongodb } = require('../application.config')

const topology = {
  exchanges: [
    { name: 'eventstore.exchange', type: 'direct' },
    { name: 'eventstore.deadletter.exchange', type: 'direct' }
  ],
  queues: [
    { name: 'eventstore.deadletter.queue' },
    { name: 'eventstore.events.queue', deadLetterExchange: 'eventstore.deadletter.exchange', deadLetterRoutingKey: 'eventstore.deadletter.key' }
  ],
  bindings: [
    { exchange: 'eventstore.deadletter.exchange', target: 'eventstore.deadletter.queue', keys: 'eventstore.deadletter.key' },
    { exchange: 'eventstore.exchange', target: 'eventstore.events.queue', keys: 'eventstore.events.key' }
  ]
}

module.exports = {
  name: 'Eventstore',
  mixins: [DbService],
  settings: {
    idField: 'id'
  },
  adapter: new MongoDBAdapter(`mongodb://${mongodb.hostname}:${mongodb.port}/warboard`, {
    keepAlive: true,
    useUnifiedTopology: true,
    auth: {
      user: mongodb.username,
      password: mongodb.password
    }
  }),
  collection: 'eventstore',
  created () {
    return false
    const rabbit = new Rabbit(`amqp://${rabbitmq.username}:${rabbitmq.password}@${rabbitmq.hostname}:${rabbitmq.port}`, {
      prefetch: 1,
      replyPattern: false,
      scheduledPublish: false,
      prefix: '',
      socketOptions: {}
    })
    rabbit.on('connected', () => {
      console.log('rabbitmq connected')
      // Create queues, add handlers etc.
      // This will be called on every reconnection too
      /* if (topology.queues.length > 0) {
        do {
          const queue = topology.queues.shift()
          const options = { durable: true, autoDelete: false }
          if (queue.deadLetterExchange) { options.deadLetterExchange = queue.deadLetterExchange }
          if (queue.deadLetterRoutingKey) { options.deadLetterRoutingKey = queue.deadLetterRoutingKey }
          rabbit.createQueue(queue.name, options, async (msg, ack) => {
          }).catch((err) => { console.log(err) })
          rabbit.bindToExchange(queue.name, 'amq.topic', 'routingKey')
        } while (topology.queues.length > 0)
      } */
    })
  },
  events: {
    'Eventstore.InsertEvent': async function (data) {
      this.broker.logger.info(data)
      await this.broker.call('Eventstore.insert', { entity: data })
    }
  }
}
