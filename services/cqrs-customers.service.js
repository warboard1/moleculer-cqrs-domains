const CQRSMixin = require('../cqrs/cqrs.mixin')

module.exports = {
  name: 'EditorDomain',
  mixins: [CQRSMixin],
  hooks: {
    before: {
      '*': ['ExtractActionMetadata', 'ValidateCQRSParams']
    },
    after: {
      '*': ['EmitEventSourcing', 'ResponseCQRS']
    },
    error: {
      '*': ['ErrorCQRS']
    }
  },
  actions: {
    // Commands
    CreateCustomerCommand: require('../cqrs/domains/customers/commands/CreateCustomerCommand').getAction(),
    UpdateCustomerByIdCommand: require('../cqrs/domains/customers/commands/UpdateCustomerByIdCommand').getAction(),
    // Queries
    SearchCustomersQuery: require('../cqrs/domains/customers/queries/SearchCustomersQuery').getAction()
  },
  methods: {
  }
}
