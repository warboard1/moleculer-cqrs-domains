const DbService = require('moleculer-db')
const SqlAdapter = require('moleculer-db-adapter-sequelize')
const Sequelize = require('sequelize')

const { postgres } = require('../application.config')

module.exports = {
  name: 'CustomersAggregator',
  mixins: [DbService],
  settings: {
    idField: 'id'
  },
  adapter: new SqlAdapter('warboard', postgres.username, postgres.password, {
    dialect: 'postgres',
    host: postgres.hostname,
    port: postgres.port
  }),
  model: {
    name: 'customers',
    define: {
      id: {
        type: Sequelize.UUID,
        primaryKey: true
      },
      email: {
        type: Sequelize.STRING,
        unique: true
      },
      password: Sequelize.STRING,
      firstname: Sequelize.STRING,
      lastname: Sequelize.STRING,
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE
    },
    options: {
      timestamps: false
    }
  }
}
