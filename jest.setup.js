const eventstore = require('./cqrs/eventstore')

const run = async () => {
  const { r, conn } = await eventstore()
  const exists = await r.dbList().contains('test').run(conn)
  if (exists === true) {
    await r.dbDrop('test').run(conn)
  }
  await r.dbCreate('test').run(conn)
  await r.db('test').tableCreate('eventstore').run(conn)
  await r.db('test').tableCreate('heroes').run(conn)

  process.exit(0)
}

run()
