const nconf = require('nconf')
nconf.argv().env().file({ file: 'nconf.json' })

// ************************************
// Typecasting from kube env
// ************************************
let APP_NATS_PORT = 4222
let APP_RABBITMQ_PORT = 5672
let APP_POSTGRES_PORT = 5432
let APP_MONGODB_PORT = 27017
// ************************************
if (nconf.get('APP_NATS_PORT')) { APP_NATS_PORT = parseInt(nconf.get('APP_NATS_PORT')) }
if (nconf.get('APP_RABBITMQ_PORT')) { APP_RABBITMQ_PORT = parseInt(nconf.get('APP_RABBITMQ_PORT')) }
if (nconf.get('APP_POSTGRES_PORT')) { APP_POSTGRES_PORT = parseInt(nconf.get('APP_POSTGRES_PORT')) }
if (nconf.get('APP_MONGODB_PORT')) { APP_MONGODB_PORT = parseInt(nconf.get('APP_MONGODB_PORT')) }
// ************************************

const APP_NATS_HOSTNAME = nconf.get('APP_NATS_HOSTNAME') || 'nats.docker.localhost'
const APP_NATS_USERNAME = nconf.get('APP_NATS_USERNAME') || 'infra'
const APP_NATS_PASSWORD = nconf.get('APP_NATS_PASSWORD') || 'infra'

const APP_RABBITMQ_HOSTNAME = nconf.get('APP_RABBITMQ_HOSTNAME') || 'rabbitmq.docker.localhost'
const APP_RABBITMQ_USERNAME = nconf.get('APP_RABBITMQ_USERNAME') || 'infra'
const APP_RABBITMQ_PASSWORD = nconf.get('APP_RABBITMQ_PASSWORD') || 'infra'

const APP_POSTGRES_HOSTNAME = nconf.get('APP_POSTGRES_HOSTNAME') || 'postgres.docker.localhost'
const APP_POSTGRES_USERNAME = nconf.get('APP_POSTGRES_USERNAME') || 'infra'
const APP_MONGODB_PASSWORD = nconf.get('APP_MONGODB_PASSWORD') || 'infra'

const APP_MONGODB_HOSTNAME = nconf.get('APP_MONGODB_HOSTNAME') || 'mongodb.docker.localhost'
const APP_MONGODB_USERNAME = nconf.get('APP_MONGODB_USERNAME') || 'infra'
const APP_POSTGRES_PASSWORD = nconf.get('APP_POSTGRES_PASSWORD') || 'infra'

module.exports = {
  nats: {
    hostname: APP_NATS_HOSTNAME,
    port: APP_NATS_PORT,
    username: APP_NATS_USERNAME,
    password: APP_NATS_PASSWORD
  },
  rabbitmq: {
    hostname: APP_RABBITMQ_HOSTNAME,
    port: APP_RABBITMQ_PORT,
    username: APP_RABBITMQ_USERNAME,
    password: APP_RABBITMQ_PASSWORD
  },
  postgres: {
    hostname: APP_POSTGRES_HOSTNAME,
    port: APP_POSTGRES_PORT,
    username: APP_POSTGRES_USERNAME,
    password: APP_POSTGRES_PASSWORD
  },
  mongodb: {
    hostname: APP_MONGODB_HOSTNAME,
    port: APP_MONGODB_PORT,
    username: APP_MONGODB_USERNAME,
    password: APP_MONGODB_PASSWORD
  }
}
