const { Query } = require('../../../utils')

const handler = async function (ctx) {
  const result = await ctx.broker.call('CustomersAggregator.list', ctx.params)
  return result
}

const query = new Query()
query.setHandler(handler)
query.setModel('cqrs/domains/customers/models/CustomersSearchModel')

module.exports = query
