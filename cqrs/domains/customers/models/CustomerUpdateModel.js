const Joi = require('@hapi/joi')

const Customer = require('./Customer')

const schema = Joi.object({
  id: Customer.id.required(),
  email: Customer.email,
  firstname: Customer.firstname,
  lastname: Customer.lastname
})

module.exports = schema
