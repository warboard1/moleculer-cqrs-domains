const Joi = require('@hapi/joi')

const schema = Joi.object({
  search: Joi.string().required(),
  searchFields: Joi.array().items(Joi.string()).required(),
  page: Joi.number().integer(),
  pagSize: Joi.number().integer()
})

module.exports = schema
