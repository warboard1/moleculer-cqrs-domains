const Joi = require('@hapi/joi')

const Customer = require('./Customer')

const schema = Joi.object({
  email: Customer.email.required(),
  password: Customer.password.required(),
  password_confirm: Customer.password.required()
})

module.exports = schema
