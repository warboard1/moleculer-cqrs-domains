const Joi = require('@hapi/joi')

const schema = {
  id: Joi.string().guid({ version: ['uuidv4'] }),
  email: Joi.string().email().min(3).max(50),
  password: Joi.string().min(8).max(50),
  firstname: Joi.string().min(2).max(255),
  lastname: Joi.string().min(2).max(255)
}

module.exports = schema
