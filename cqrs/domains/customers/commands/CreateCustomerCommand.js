const argon2 = require('argon2')

const { Command } = require('../../../utils')

const handler = async function (ctx) {
  // Extract some data
  const { password, password_confirm: confirm } = ctx.params
  // Generate uuid
  ctx.params.id = ctx.broker.generateUid()
  // Control password === password_confirm
  if (password !== confirm) {
    throw new Error('ERROR__PASSWORDS_NOT_MATCHED')
  }
  // Hash password
  const options = {
    timeCost: 4, memoryCost: 2 ** 13, parallelism: 2, type: argon2.argon2id
  }
  const hash = await argon2.hash(password, options)
  ctx.params.password = hash
  delete ctx.params.password_confirm
  // Save Projection
  const createdAt = new Date()
  ctx.params = {
    ...ctx.params,
    createdAt,
    updatedAt: createdAt
  }
  await ctx.broker.call('CustomersAggregator.create', ctx.params)

  return {
    aggregateID: ctx.params.id
  }
}

const command = new Command()
command.setHandler(handler)
command.setModel('cqrs/domains/customers/models/CustomerCreateModel')
command.setEventType('CustomerCreatedEvent')
command.setAggregateType('Customer')

module.exports = command
