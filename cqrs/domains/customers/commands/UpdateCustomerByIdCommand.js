const { Command } = require('../../../utils')

const handler = async function (ctx) {
  // Save Projection
  await ctx.broker.call('CustomersAggregator.update', ctx.params)
  return {
    aggregateID: ctx.params.id
  }
}

const command = new Command()
command.setHandler(handler)
command.setModel('cqrs/domains/customers/models/CustomerUpdateModel')
command.setEventType('CustomerUpdatedEvent')
command.setAggregateType('Customer')

module.exports = command
