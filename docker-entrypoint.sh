#!/bin/bash

# Environment
echo "Environment: $NODE_ENV"

# Start application
echo "Start application"
exec yarn start
