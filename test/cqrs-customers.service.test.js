const { ServiceBroker } = require('moleculer')
const broker = new ServiceBroker({
  logger: false,
  metrics: {
    enabled: false
  }
})

beforeAll(async () => {
  await broker.loadService('./services/cqrs-customers.service')
  await broker.loadService('./services/eventstore.service')
  await broker.loadService('./services/customers-aggregator.service')
  await broker.start()
})

afterAll(async () => {
  await broker.stop()
})

describe('EditorDomain.CreateCustomerCommand', () => {
  test('should failed to create a new customer, because no params', async () => {
    const { status, error: { message } } = await broker.call('EditorDomain.CreateCustomerCommand')
    expect(status).toEqual(false)
    expect(message).toEqual('"email" is required')
  })
  test('should failed to create a new customer, because the password not matched', async () => {
    const { status, error: { message } } = await broker.call('EditorDomain.CreateCustomerCommand', {
      email: 'customerTest01@gmail.com',
      password: '12345678',
      password_confirm: 'AZERQSDF'
    })
    expect(status).toEqual(false)
    expect(message).toEqual('ERROR__PASSWORDS_NOT_MATCHED')
  })
  test('should failed to create a new customer, because email already in database', async () => {
    const { status, error: { message } } = await broker.call('EditorDomain.CreateCustomerCommand', {
      email: 'customerTest02@gmail.com',
      password: 'AZERQSDF',
      password_confirm: 'AZERQSDF'
    })
    expect(status).toEqual(false)
    expect(message).toEqual('ERROR__CUSTOMER_ALREADY_EXISTS')
  })
  test('should successfully create a new customer', async () => {
    const { status } = await broker.call('EditorDomain.CreateCustomerCommand', {
      email: 'customerTest01@gmail.com',
      password: 'AZERQSDF',
      password_confirm: 'AZERQSDF'
    })
    expect(status).toEqual(true)
  })
})

describe('EditorDomain.UpdateCustomerByIdCommand', () => {
  test('should failed to update customer because id is mandatory', async () => {
    const { status, error: { message } } = await broker.call('EditorDomain.UpdateCustomerByIdCommand')
    expect(status).toEqual(false)
    expect(message).toEqual('"id" is required')
  })
  test('should failed to update customer because customer not found', async () => {
    const { status, error: { message } } = await broker.call('EditorDomain.UpdateCustomerByIdCommand', {
      id: '98bea2e9-e3f0-48ed-b9d0-8e1b0282a043',
      firstname: 'test01_not_found'
    })
    expect(status).toEqual(false)
    expect(message).toEqual('ERROR__CUSTOMER_NOT_FOUND')
  })
  test('should successfully update a customer', async () => {
    const { status } = await broker.call('EditorDomain.UpdateCustomerByIdCommand', {
      id: '98bea2e9-e3f0-48ed-b9d0-8e1b0282a043',
      firstname: 'test01_ok'
    })
    expect(status).toEqual(true)
  })
})

describe('EditorDomain.SearchCustomersQuery', () => {
  test('should failed to search customer because search is mandatory', async () => {
    const { status, error: { message } } = await broker.call('EditorDomain.SearchCustomersQuery')
    expect(status).toEqual(false)
    expect(message).toEqual('"search" is required')
  })
  test('should successfullyfound a customer', async () => {
    const { status } = await broker.call('EditorDomain.SearchCustomersQuery', {
      search: 'test01',
      searchFields: ['name']
    })
    expect(status).toEqual(true)
  })
})
