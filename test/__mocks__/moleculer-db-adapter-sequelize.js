class SqlAdapter {
  init (broker, service) {
    this.broker = broker
    this.service = service
  }

  connect () {
    const modelReadyPromise = Promise.resolve()
    return modelReadyPromise.then(() => {
      this.service.logger.info('Sequelize adapter has connected successfully.')
    })
  }

  beforeSaveTransformID (entity, idField) {
    return entity
  }

  async insert (data) {
    const { email = 'NULL' } = data
    if (email === 'customerTest01@gmail.com') { return true }
    if (email === 'customerTest02@gmail.com') { throw new Error('ERROR__CUSTOMER_ALREADY_EXISTS') }
    throw new Error('ERROR_NOT_REGISTERED')
  }

  async updateById (id, data) {
    const { $set: { firstname = 'NULL' } = {} } = data
    if (firstname === 'test01_ok') { return true }
    if (firstname === 'test01_not_found') { throw new Error('ERROR__CUSTOMER_NOT_FOUND') }
    throw new Error('ERROR_NOT_REGISTERED')
  }

  async find (data) {
    const { search = 'NULL' } = data
    if (search === 'test01') { return true }
    throw new Error('ERROR_NOT_REGISTERED')
  }

  async count (data) {
    return 0
  }
}

module.exports = SqlAdapter
