class MongoDBAdapter {
  init (broker, service) {
    this.broker = broker
    this.service = service
  }

  connect () {
    const modelReadyPromise = Promise.resolve()
    return modelReadyPromise.then(() => {
      this.service.logger.info('MongoDb adapter has connected successfully.')
    })
  }
}

module.exports = MongoDBAdapter
